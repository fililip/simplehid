# SimpleHID - a rather... simple hidapi wrapper for C#

requires the `hidapi` library. duh

(works on linux *and* windows b t w!)

### why
because it's nice to have a lightweight HID wrapper that you can *actually* use from C#

### help
an example app can be found [here](src/Program.cs)

thank

ok btw,

## windows®®™™ usage instructions:
- download `hidapi-win.zip` from [here](https://github.com/libusb/hidapi/releases)
- put `hidapi.dll` in the executable file's directory
- change the name to `libhidapi-hidraw.dll` (or optionally use a different `libraryName` in [HID.cs](src/HID.cs))
- enjoy

thank. for real this time