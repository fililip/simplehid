﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace SimpleHID
{
	public struct HIDDescriptor
	{
		public string path;
		public ushort vendorID;
		public ushort productID;
		public string serialNumber;
		public ushort releaseNumber;
		public string manufacturerString;
		public string productString;
		public ushort usagePage;
		public ushort usage;
		public int interfaceNumber;
	}

	public class HID
	{
		const string libraryName = "libhidapi-hidraw";

		[DllImport(libraryName)]
		static extern int hid_init();

		[DllImport(libraryName)]
		static extern IntPtr hid_enumerate(ushort vid, ushort pid);

		[DllImport(libraryName)]
		static extern IntPtr hid_open(ushort vid, ushort pid, string serial);

		[DllImport(libraryName)]
		static extern int hid_get_manufacturer_string(IntPtr device, IntPtr stringPointer, int size);

		[DllImport(libraryName)]
		static extern int hid_get_product_string(IntPtr device, IntPtr stringPointer, int size);

		[DllImport(libraryName)]
		static extern int hid_get_serial_number_string(IntPtr device, IntPtr stringPointer, int size);

		[DllImport(libraryName)]
		static extern int hid_get_input_report(IntPtr device, byte[] data, int size);

		[DllImport(libraryName)]
		static extern int hid_get_feature_report(IntPtr device, byte[] data, int size);

		[DllImport(libraryName)]
		static extern int hid_read(IntPtr device, byte[] data, int size);

		[DllImport(libraryName)]
		static extern int hid_write(IntPtr device, byte[] data, int size);

		[DllImport(libraryName)]
		static extern void hid_close(IntPtr device);

		[DllImport(libraryName)]
		static extern int hid_exit();

		[StructLayout(LayoutKind.Sequential)]
		struct hid_device_info
		{
			public string path;
			public ushort vendor_id;
			public ushort product_id;
			public IntPtr serial_number;
			public ushort release_number;
			public IntPtr manufacturer_string;
			public IntPtr product_string;
			public ushort usage_page;
			public ushort usage;
			public int interface_number;

			// wip
			IntPtr next;
		}

		IntPtr device = IntPtr.Zero;

		/// <summary>
		/// Initialize the HID library.
		/// </summary>
		public static void InitializeHID() => hid_init();

		/// <summary>
		/// Deinitialize the HID library.
		/// </summary>
		public static void DeinitializeHID() => hid_exit();

		static string GetString(IntPtr stringPointer)
		{
			const int size = byte.MaxValue;
			byte[] buffer = new byte[size];
			Marshal.Copy(stringPointer, buffer, 0, size);

			string returnValue = "";

			for (int i = 0; i < buffer.Length; i += 4)
			{
				if (buffer[i] == '\0') break;
				returnValue += (char)buffer[i];
			}

			return returnValue;
		}

		/// <summary>
		/// Gets information about an HID device identified with its vendor and product IDs.
		/// </summary>
		/// <param name="vid">Vendor ID</param>
		/// <param name="pid">Product ID</param>
		/// <returns>HID Descriptor</returns>
		public static HIDDescriptor GetDeviceInfo(ushort vid, ushort pid)
		{
			IntPtr devices = hid_enumerate(vid, pid);
			object infoPointer = Marshal.PtrToStructure(devices, typeof(hid_device_info));
			if (infoPointer == null) throw new Exception($"HID device {Convert.ToString(vid, 16)}:{Convert.ToString(pid, 16)} could not be found.");

			hid_device_info info = (hid_device_info)infoPointer;
			HIDDescriptor descriptor = new HIDDescriptor();

			descriptor.path = info.path;
			descriptor.vendorID = info.vendor_id;
			descriptor.productID = info.product_id;
			descriptor.serialNumber = GetString(info.serial_number);
			descriptor.releaseNumber = info.release_number;
			descriptor.manufacturerString = GetString(info.manufacturer_string);
			descriptor.productString = GetString(info.product_string);
			descriptor.usagePage = info.usage_page;
			descriptor.usage = info.usage;
			descriptor.interfaceNumber = info.interface_number;

			Marshal.FreeHGlobal(devices);

			return descriptor;
		}

		/// <summary>
		/// Connects to an HID device using its vendor and product IDs (and optionally its serial number).
		/// </summary>
		/// <param name="vid">Vendor ID</param>
		/// <param name="pid">Product ID</param>
		/// <param name="serial">Serial number (optional)</param>
		public HID(ushort vid, ushort pid, string serial = null)
		{
			device = hid_open(vid, pid, serial);
			if (device == IntPtr.Zero) throw new Exception($"HID device {Convert.ToString(vid, 16)}:{Convert.ToString(pid, 16)} could not be found.");
		}

		string GetString(Action<IntPtr, int> lambda)
		{
			const int size = byte.MaxValue;
			IntPtr stringPointer = Marshal.AllocHGlobal(size * sizeof(char));
			lambda(stringPointer, size);
			byte[] s = new byte[size];
			Marshal.Copy(stringPointer, s, 0, size);
			Marshal.FreeHGlobal(stringPointer);
			return Encoding.ASCII.GetString(s);
		}

		/// <summary>
		/// Returns the manufacturer string.
		/// </summary>
		/// <returns>Manufacturer string</returns>
		public string GetManufacturerString()
		{
			return GetString((ptr, size) => { hid_get_manufacturer_string(device, ptr, size); });
		}

		/// <summary>
		/// Returns the product string.
		/// </summary>
		/// <returns>Product string</returns>
		public string GetProductString()
		{
			return GetString((ptr, size) => { hid_get_product_string(device, ptr, size); });
		}

		/// <summary>
		/// Returns the serial number string.
		/// </summary>
		/// <returns>Serial number string</returns>
		public string GetSerialNumberString()
		{
			return GetString((ptr, size) => { hid_get_serial_number_string(device, ptr, size); });
		}

		/// <summary>
		/// Reads input data from the HID device.
		/// </summary>
		/// <returns>Input data</returns>
		public byte[] Read()
		{
			byte[] data = new byte[128];
			hid_read(device, data, data.Length);
			return data;
		}

		/// <summary>
		/// Writes output data to the HID device.
		/// </summary>
		/// <param name="data">Buffer containing the output data</param>
		public void Write(byte[] data)
			=> hid_write(device, data, data.Length);

		/// <summary>
		/// Closes the HID device.
		/// </summary>
		public void Close() => hid_close(device);
	}
}