using System;
using System.Threading;

namespace SimpleHID
{
	// dualshock 4 example
	
	class Program
	{
		static void Main(string[] args)
		{
			// call this before using the wrapper
			HID.InitializeHID();

			// open the HID device
			HID device = new HID(0x054c, 0x05c4);

			// allocate an output buffer for things like the RGB LED and rumble
			byte[] output = new byte[14];

			// sony stuff
			output[0] = 0x05;
			output[1] = 0xFF;

			Console.WriteLine("Press any key to exit");

			// animate some parameters
			ulong time = 0;
			while (!Console.KeyAvailable)
			{
				// read controller input
				byte[] input = device.Read();
				Console.Write($"button_state: {input[5]}  \r");

				// weaker rumble
				output[4] = (byte)(Math.Sin(time / 40.0) * 64 + 64);

				// stronger rumble
				output[5] = (byte)(Math.Sin(time / 40.0) * 64 + 64);

				// RGB LED
				output[6] = (byte)(Math.Abs((float)((time / 1.0) % 255) - 127.5));
				output[7] = (byte)(Math.Abs((float)((time / 2.0) % 255) - 127.5));
				output[8] = (byte)(Math.Abs((float)((time / 3.0) % 255) - 127.5));

				device.Write(output);
				Thread.Sleep(1);
				time++;
			}

			Console.WriteLine();

			// don't forget to close your device so that the OS regains control over it
			device.Close();

			// and once you're done with the wrapper, use the following method
			HID.DeinitializeHID();
		}
	}
}